<?php
/**
* @version		1.5.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

/************** Bahasa Indonesia *************/
@define ('User_desc',"Apps untuk mengelola member.");
@define ('AccessDenied',"You are not allowed to access this page.<br>Please login first by login page.");
@define('WelcomeUser__','Selamat datang');
@define('ThisLink__','link ini');
@define('CangePass__','ganti password');
@define('Sure2Logout__','Anda yakin ingin keluar? ');
@define('RegisterNotAllowed__','Maaf, pendaftaran member baru di tutup!');
@define('InvalidData__','Data yang anda masukan salah !');
@define('PasswordSent__','Password baru telah dikirim ke email anda.');
@define('user_Login_Success','Anda telah login dan berhak mengakses halaman khusus member.');
@define('user_Password_Reminder','Kami akan mengirim data akun ke email Anda.');
@define('user_Please_fill_the_fields','Mohon lengkapi data terlebih dahulu!');
@define("user_Password_Not_Match","Konfirmasi password salah!");
