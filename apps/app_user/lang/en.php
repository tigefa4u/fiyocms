<?php
/**
* @version		1.5.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

/************** English *************/
@define ('User_desc',"Manages your member on site");
@define ('AccessDenied',"You are not allowed to access this page.<br>Please login first by login page.");
@define ('WelcomeUser__', 'Welcome');
@define ('ThisLink__', 'this link');
@define ('CangePass__', 'change password');
@define ('Sure2Logout__', 'Are you sure you want to quit?');
@define ('RegisterNotAllowed__', 'Sorry, new member registration is closed!');
@define ('InvalidData__', 'The data you enter is wrong!');
@define ('PasswordSent__', 'A new password has been sent to your email.');
@define ('user_Login_Success', 'You have logged in and authorized to access special members page.');
@define ('user_Password_Reminder', 'We will send the data to your email account.');
@define ('user_Please_fill_the_fields', 'Please fill out the data first!');
@define ("user_Password_Not_Match", "Confirm password is incorrect!");